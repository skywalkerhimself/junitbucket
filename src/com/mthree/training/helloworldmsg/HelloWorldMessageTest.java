package com.mthree.training.helloworldmsg;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class HelloWorldMessageTest 
{

	static private HelloWorldMessage objectUnderTest;
	
	@BeforeAll
	static void setupBeforeClass()
	{
		objectUnderTest = new HelloWorldMessage();
	}
	
	@Test
	void test_newInstance() 
	{
		assertNotNull(objectUnderTest);
		
	}
	
	@Test
	void test_getMessage() 
	{
		assertNotNull(objectUnderTest.getMessage());
		
	}
	
	@Test
	void test_getMessage_twoUniqueMessage() 
	{
		
		String firstResult = objectUnderTest.getMessage();
		String secondResult = objectUnderTest.getMessage();
		String errorMessage = "Object Under Test could not produce two unique results." 
				+ " [" + firstResult + "] " 
				+ " [" + secondResult+ "] ";
		assertNotEquals(firstResult, secondResult, errorMessage);
		
	}
}
